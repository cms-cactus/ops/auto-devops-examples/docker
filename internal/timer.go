package internal

import (
	"time"
)

// Timer does time things
type Timer interface {
	report() string
}

// RuntimeTimer keeps track of how long we've been running
type RuntimeTimer struct {
	startTime time.Time
}

// MakeRuntimeTimer keeps track of how long we've been running
func MakeRuntimeTimer() RuntimeTimer {
	return RuntimeTimer{time.Now()}
}

func (t RuntimeTimer) report() string {
	return time.Now().Sub(t.startTime).String()
}
