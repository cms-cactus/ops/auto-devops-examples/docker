[![pipeline status](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/docker/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/-/commits/master)

## GitLab Cactus AutoDevops basic docker example

This implements the `docker-builder` template from the [AutoDevOps](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/) repo.