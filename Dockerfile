FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-c8:tag-0.0.3 AS builder
LABEL maintainer="Cactus <cactus@cern.ch>"

ENV GO_VERSION=1.14.7
ENV PATH="$PATH:/usr/local/go/bin:/root/go/bin"

RUN curl -Lo go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz && \
    go get -u golang.org/x/tools/... && \
    go get -u github.com/jstemmer/go-junit-report


WORKDIR /root/
COPY cmd cmd
COPY internal internal
COPY go.mod .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /serve ./cmd/serve.go

FROM scratch
COPY --from=builder /serve /bin/serve
ENTRYPOINT ["/bin/serve"]
EXPOSE 80